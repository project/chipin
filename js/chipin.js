
/**
 * Browser detection script originally created
 * by Peter Paul Koch at http://www.quirksmode.org/
 */
function getBrowserInfo()
{
	agt = navigator.userAgent.toLowerCase();
	is_win = ((agt.indexOf("win") != -1) ||
		(agt.indexOf("16bit") != -1));
	is_mac = (agt.indexOf("mac") != -1);
	is_unix = (agt.indexOf("x11") != -1);
	if (checkIt('konqueror')) browser = "Konqueror";
	else if (checkIt('safari')) browser 	= "Safari";
	else if (checkIt('omniweb')) browser 	= "OmniWeb";
	else if (checkIt('opera')) browser 	= "Opera";
	else if (checkIt('webtv')) browser 	= "WebTV";
	else if (checkIt('icab')) browser 	= "iCab";
	else if (checkIt('msie')) browser 	= "Internet Explorer";
	else if (!checkIt('compatible')) browser = "Netscape Navigator or Firefox/Mozilla";
	else browser = "Unknown browser";

	return browser;
}

function checkIt(string)
{
	browserDetectString = navigator.userAgent.toLowerCase();
	return browserDetectString.indexOf(string) + 1;
}

var is_mac = false;
var is_unix = false;
var is_ie = false;
var is_win = false;
var is_safari = false;
var browserType = getBrowserInfo();
if (browserType == "Internet Explorer")
	is_ie = true;
else if (browserType == "Safari")
	is_safari = true;


function addEvent(obj, evType, fn)
{
	if (obj.addEventListener) {
		obj.addEventListener(evType, fn, true);
		return true;
	} else if (obj.attachEvent) {
		var r = obj.attachEvent("on" + evType, fn);
		return r;
	} else {
		return false;
	}
}

/**
 * ChipIn widget wizard code
 */
noFormFocus = true;

var currentcolorid = "";
var server = "www.chipin.com";
var protocol = "https://";

function setWidget()
{
	frameObj = document.getElementById("widgetframe");
	demostr = protocol + server + "/widget/widget.swf?";

	event_id = document.getElementById("edit-event_id").value;
	if (event_id != "")
		demostr += "event_id=" + event_id;
	else
		demostr += "use_sample_data=true";

	titleObj = document.getElementById("edit-event_title").value;
	if (titleObj != "")
		demostr += "&event_title=" + escape(titleObj);
	desc = document.getElementById("edit-event_desc").value;
	if (desc != "")
		demostr += "&event_desc=" + escape(desc);
	bord = document.getElementById("edit-showborder");
	if (bord.checked == false)
		demostr += "&show_border=false";
	bar = document.getElementById("edit-showbar");
	if (bar.checked == false)
		demostr += "&show_progressbar=false";
	eimg = document.getElementById("edit-showevent");
	if (eimg.checked == false)
		demostr += "&show_eventimage=false";
	bg = document.getElementById("edit-showbg");
	if (bg.checked == false)
		demostr += "&show_bgimage=false";
	desc = document.getElementById("edit-showdesc");
	if (desc.checked == false)
		demostr += "&show_eventdesc=false";

	demostr += checkColor("color_title", "colortitle");
	demostr += checkColor("color_graphfg", "colorbar");
	demostr += checkColor("color_fg", "colordesc");
	demostr += checkColor("color_graphbg", "colorbarb");
	demostr += checkColor("color_border", "colorborder");
	demostr += checkColor("color_info", "colorbart");
	demostr += checkColor("color_bg", "colorbg");

	w = document.getElementById("edit-widget_width").value;
	h = document.getElementById("edit-widget_height").value;
	align = document.getElementById("edit-widget_align").value;
	
	optionstr = "";

	newstr = "";
	pos = demostr.indexOf("?");
	params = demostr.substr(pos + 1);
	optionstr = params.replace(/\&/g, "|");
	
	if (align != "")
		optionstr += "|align=" + align;
	
	demostr += "&chipin_server=" + server;

	frameObj.setAttribute("src", demostr);

	dw = w;
	dh = h;
	if (w == "100%")
		dw = 280;
	if (h == "100%")
		dh = 320;
	frameObj.setAttribute("width", dw);
	frameObj.setAttribute("height", dh);

	document.getElementById("edit-widgettag").value = "[chipin|" + optionstr + "|width=" + w + "|height=" + h + "]";
}

function splitParams(params)
{
	newstr = "";
	pos = params.indexOf("?");
	params = params.substr(pos + 1);
	paramArray = params.split("&");
	for (i = 0; i < paramArray.length; i++) {
		nameval = paramArray[i];
		parts = nameval.split("=");
		n = parts[0];
		v = parts[1];
		newstr += (" " + n + "=\"" + v + "\"");
	}
	return newstr;
}

function checkColor(param, basestr)
{
	fobj = document.getElementById("edit-" + basestr + "f");
	dobj = document.getElementById("edit-" + basestr + "d");
	colstr = fobj.value;
	if (isColor(colstr)) {
		dobj.style.backgroundColor = "#" + colstr;
		return "&" + param + "=" + colstr;
	}
	return "";
}

function isColor(testcolor)
{
	if (testcolor == "")
		return false;
	matches = testcolor.match(/([a-fA-F0-9]{6})/g);
	if (matches)
		return true;
	return false;
}

function isValidEventID(testeventid)
{
	validID = false;
	if (testeventid && testeventid.length >= 16) {
		matches = testeventid.match(/([a-zA-Z0-9]{16})/g);
		if (matches)
	    validID = true;
	}
	return validID;
}

function setWidgetLayout(num)
{
	if (num == 1) {
		setColor("colortitle", "ffffff");
		setColor("colorbar", "758f46");
		setColor("colordesc", "000000");
		setColor("colorbarb", "dddddd");
		setColor("colorborder", "fcab42");
		setColor("colorbart", "758f46");
		setColor("colorbg", "ffffff");
	} else if (num == 2) {
		setColor("colortitle", "ffffff");
		setColor("colorbar", "5a7734");
		setColor("colordesc", "000000");
		setColor("colorbarb", "fdf7d5");
		setColor("colorborder", "ddb931");
		setColor("colorbart", "5a7734");
		setColor("colorbg", "f3e09c");
	} else if (num == 3) {
		setColor("colortitle", "ffffff");
		setColor("colorbar", "597332");
		setColor("colordesc", "000000");
		setColor("colorbarb", "d0d7b6");
		setColor("colorborder", "597332");
		setColor("colorbart", "000000");
		setColor("colorbg", "fdf7d5");
	}
	setWidget();
}

function setColor(basestr, col)
{
	fobj = document.getElementById("edit-" + basestr + "f");
	fobj.value = col;
}

function chooseColor(e)
{
	imgObj = document.getElementById("spectrumimg");
	htmlObj = document.getElementsByTagName("html")[0];
	bodyObj = document.body;

	ix = findPosX(imgObj);
	iy = findPosY(imgObj);

            if (is_ie) {
		offX = (findWidth(htmlObj) - findWidth(bodyObj)) / 2;
		if (offX < 0)
			offX = 12;
		offY = document.documentElement.scrollTop;
                    coordX = e.clientX - (ix + offX) +
			document.documentElement.scrollLeft;
		if (coordX < 0)
			coordX = 0;
                    coordY = e.clientY + offY;
		coordY -= iy;
            } else {
                    coordX = e.pageX;
                    coordY = e.pageY;
		coordX -= ix;
		coordY -= iy;
            }

	sw = 304;
	sh = 109;
	bh = 10;
	hex = "";

	if (coordY <= bh) {
		percent = coordX / sw;
		if (percent > 1)
			percent = 1;
		r = Math.round(percent * 255.0);
		g = Math.round(percent * 255.0);
		b = Math.round(percent * 255.0);
		hex = rgb2hex(r, g, b);
	} else {
		coordY -= bh;
		percentX = coordX / sw;
		percentY = coordY / (sh - bh);
		h = (percentX * 360.0);
		if (percentY <= .5) {
			v = 1;
			s = percentY * 2;
			if (s > 1)
				s = 1;
		} else {
			s = 1;
			v = 1 - ((percentY - .5) * 2);
			if (v < 0)
				v = 0;
		}
		hex = hsv2web(h, s, v);
	}

	fObj = document.getElementById("edit-" + currentcolorid + "f");
	fObj.value = hex;
	dObj = document.getElementById("edit-" + currentcolorid + "d");
	dObj.style.backgroundColor = "#" + hex;
	setWidget();
}

function hsv2rgb(h, s, v)
{
	if (s == 0)
		return new Array(v, v, v);
	h = h / 60.0;
	i = Math.floor(h);
	f = h - i;
	p = v * (1 - s);
	q = v * (1 - s * f);
	t = v * (1 - s * (1 - f));
	if (i == 0)
		return new Array(v, t, p);
	else if (i == 1)
		return new Array(q, v, p);
	else if (i == 2)
		return new Array(p, v, t);
	else if (i == 3)
		return new Array(p, q, v);
	else if (i == 4)
		return new Array(t, p, v);
	else
		return new Array(v, p, q);
}

function hsv2web(h, s, v)
{
	rgbcolor = hsv2rgb(h, s, v);
	r = Math.round(rgbcolor[0] * 255.0);
	g = Math.round(rgbcolor[1] * 255.0);
	b = Math.round(rgbcolor[2] * 255.0);
	w1 = r.toString(16);
	if (w1.length == 1)
		w1 = "0" + w1;
	w2 = g.toString(16);
	if (w2.length == 1)
		w2 = "0" + w2;
	w3 = b.toString(16);
	if (w3.length == 1)
		w3 = "0" + w3;
	webcolor = w1 + w2 + w3;
	return webcolor.toUpperCase();
}

function rgb2hex(r, g, b)
{
	w1 = r.toString(16);
	if (w1.length == 1)
		w1 = "0" + w1;
	w2 = g.toString(16);
	if (w2.length == 1)
		w2 = "0" + w2;
	w3 = b.toString(16);
	if (w3.length == 1)
		w3 = "0" + w3;
	webcolor = w1 + w2 + w3;
	return webcolor.toLowerCase();
}

function closePicker()
{
	if (currentcolorid) {
		fObj = document.getElementById("edit-" + currentcolorid + "f");
		fObj.style.backgroundColor = "#ffffff";
		lObj = document.getElementById("edit-" + currentcolorid + "-label");
		lObj.style.textDecoration = "none";
	}
	$("#spectrum:visible").slideUp("slow");
}

function pickcolor(colorid)
{
	if (currentcolorid) {
		fObj = document.getElementById("edit-" + currentcolorid + "f");
		fObj.style.backgroundColor = "#ffffff";
		lObj = document.getElementById("edit-" + currentcolorid + "-label");
		lObj.style.textDecoration = "none";
	}

	currentcolorid = colorid;

	$("#spectrum:hidden").slideDown("slow");
	
	fObj = document.getElementById("edit-" + currentcolorid + "f");
	fObj.style.backgroundColor = "#ffffdd";
	lObj = document.getElementById("edit-" + currentcolorid + "-label");
	lObj.style.textDecoration = "underline";
}

function returnFilter(obj, e)
{
	key = (e.which) ? e.which : e.keyCode;
	if (key == 8)
		return true;
	else if (key <= 13) {
		if (obj.value != "")
			setWidget();
		
		if (key != 9) {
			stopEvent(obj, e);
			return false;
		}
	}
	return true;
}

function hexFilter(obj, e)
{
	key = (e.which) ? e.which : e.keyCode;
	if (key == 8 || key == 9 || key == 46)
		return true;
	if (key >= 65 && key <= 70)
		return true;
	if (key >= 97 && key <= 102)
		return true;
	if (key >= 48 && key <= 57)
		return true;
	if (key == 13) {
		if (obj.value.length == 6)
			setWidget();
		stopEvent(obj, e);
		return false;
	}

	stopEvent(obj, e);
	return false;
}

function findPosX(obj)
{
	var curleft = 0;
	if (obj.offsetParent) {
		while (obj.offsetParent) {
			curleft += obj.offsetLeft;
			obj = obj.offsetParent;
		}
	} else if (obj.x)
		curleft += obj.x;
	return curleft;
}

function findPosY(obj)
{
	var curtop = 0;
	if (obj.offsetParent) {
		while (obj.offsetParent) {
			curtop += obj.offsetTop;
			obj = obj.offsetParent;
		}
	} else if (obj.y)
		curtop += obj.y;
	return curtop;
}

function insertWidgetCode()
{
	var formObj = document.forms[0];
	var eventID = formObj['edit-event_id'].value;
  if (!isValidEventID(eventID)) {
  	alert('Please enter a valid ChipIn event ID.');
		return false;
  } else if (window.opener) {
    // Get variables from the fields on the properties frame
		var content = formObj['edit-widgettag'].value;
		insertToEditor(content);
		return false;
  } else {
		alert('The widget cannot be inserted because the parent window cannot be found.');
		return false;
	}
}

String.prototype.trim = function() { return this.replace(/^\s+|\s+$/, ''); };

addEvent(window, 'load', initWizard);
addEvent(window, 'load', setWidget);