
-----------------------
  OVERVIEW
-----------------------
The ChipIn Drupal module enables users to easily insert customized ChipIn Flash
widgets into content and blocks.

-----------------------
  ABOUT CHIPIN
-----------------------
ChipIn empowers individuals to connect with people in their social network to 
collect money for a personal cause, to purchase a gift, or for community 
fundraising. ChipIn makes this social ecommerce(sm) process of connecting and 
collecting fun, easy and secure. ChipIn believes that positive change will result 
from enabling groups to harness the power of giving. That's the ChipIn mission.

ChipIn helps people:

	* Organize and collaborate with each other to collect money for a group 
	  purchase
	* Raise money for personal and community causes
	* Collect money to make a difference for a recipient
	* Connect with people who share a common goal for collecting funds
	* Get people involved in giving together
	* Actually enjoy being an organizer
	* Account for money collected

ChipIn has also graciously offered to waive all transaction fees to help in
the Drupal.org fundraising!

-----------------------
  INSTALLATION
-----------------------
1. Place the entire chipin directory into your Drupal modules directory.

2. Enable this module by navigating to:

     administer > modules

3. Enable the ChipIn input format filter by going to:

     administer > input formats
     
   and clicking 'configure' next to the input formats you'd like to use with
   ChipIn. Then, check the 'ChipIn Flash widget' checkbox to enable the filter.

4. Set permissions for who can use the ChipIn Flash widget wizard by going to:

     administer > access control

5. Select options for the ChipIn Flash widget wizard by going to:

     administer > settings > chipin
     
6. Go to http://www.chipin.com and create a new ChipIn event, recording your
   ChipIn event ID.

7. Come back to your Drupal site, create a new post and click the ChipIn link 
   to start the wizard!

-----------------------
  TODO
-----------------------
- TinyMCE plugin.

-----------------------
  MODULE INFORMATION
-----------------------
This module was sponsored by the great folks over at ChipIn. Go check out
http://www.chipin.com/overview to learn more about ChipIn.

The ChipIn module was authored by Rob Barreca of Electronic Insight and more
information can be found at http://www.electronicinsight.com.
